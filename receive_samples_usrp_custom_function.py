"""
    Receiving
    The convenience function recv_num_samps(), is not suitable
    for any serious applications because it hides some of the
    interesting behavior going on under the hood, and there
    is some set up that happens each call that we might only
    want to do once at the beginning, e.g., if we want to receive
    samples indefinitely. The following code has the same functionality
    as recv_num_samps(), in fact it’s almost exactly what gets
    called when you use the convenience function, but now we have
    the option to modify the behavior:
"""

import uhd
import numpy as np

# Create a new usrp device
usrp = uhd.usrp.MultiUSRP()

print('Setting the reference clock source to "external"...')
usrp.set_clock_source("internal")
print(f'Clock source is now {usrp.get_clock_source(0)}\n')

# Setting time source to gpsdo
print('Setting the reference clock source to "external"...')
usrp.set_time_source("internal")
print(f'Time source is now {usrp.get_time_source(0)}\n')

num_samps = 10_000  # number of samples received
center_freq = 70e6
sample_rate = 1e6
gain = 50

#  1. Set up stream parameters

# Set sample rate
usrp.set_rx_rate(sample_rate, 0)    # (sample rate, channel 0)
# Set receive center frequency point
usrp.set_rx_freq(uhd.libpyuhd.types.tune_request(center_freq), 0)
# Set receive gain
usrp.set_rx_gain(gain, 0)
# Use daughterboard A and channel 0
# usrp.set_rx_subdev_spec("A:0", 0)
# If you want to use the TX/RX port instead of RX2 (the default)
# usrp.set_rx_antenna('TX/RX', 0)  # set channel 0 to 'TX/RX'

# 2. Set up the receive stream and receive buffer

# Create the stream args object and initialize the data formats to fc32 and sc16:
st_args = uhd.usrp.StreamArgs("fc32", "sc16")  # f32 - float, s16 - int16_t
# Set the channel list, we want 1 streamer coming from channels 0:
st_args.channels = [0]
# Set metadata
metadata = uhd.types.RXMetadata()
streamer = usrp.get_rx_stream(st_args)
buffer_samps = streamer.get_max_num_samps()
recv_buffer = np.zeros((1, buffer_samps), dtype=np.complex64)

# 3. Start Stream

# Set reception mode
stream_cmd = uhd.types.StreamCMD(uhd.types.StreamMode.start_cont)  # - start stream
stream_cmd.stream_now = True
streamer.issue_stream_cmd(stream_cmd)

# 4. Receive Samples
samples = np.zeros(num_samps, dtype=np.complex64)
for i in range(num_samps//buffer_samps):
    streamer.recv(recv_buffer, metadata)
    samples[i*buffer_samps:(i+1)*buffer_samps] = recv_buffer[0]
    print(f'i{i}: {metadata}')

# 5. Stop Stream
stream_cmd = uhd.types.StreamCMD(uhd.types.StreamMode.stop_cont)
streamer.issue_stream_cmd(stream_cmd)


print(len(samples))
print(samples[0:10])
print(usrp.get_clock_source(0))
print(usrp.get_time_source(0))

