/* -*- c++ -*- */
/*
 * Copyright 2022 gr-sdr_holography author.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef INCLUDED_SDR_HOLOGRAPHY_AVERAGE_FF_H
#define INCLUDED_SDR_HOLOGRAPHY_AVERAGE_FF_H

#include <gnuradio/sync_decimator.h>
#include <sdr_holography/api.h>

namespace gr {
namespace sdr_holography {

/*!
 * \brief <+description of block+>
 * \ingroup sdr_holography
 *
 */
class SDR_HOLOGRAPHY_API average_ff : virtual public gr::sync_decimator
{
public:
    typedef std::shared_ptr<average_ff> sptr;

    /*!
     * \brief Return a shared_ptr to a new instance of sdr_holography::average_ff.
     *
     * To avoid accidental use of raw pointers, sdr_holography::average_ff's
     * constructor is in a private implementation
     * class. sdr_holography::average_ff::make is the public interface for
     * creating new instances.
     */
    static sptr make(int length);
};

} // namespace sdr_holography
} // namespace gr

#endif /* INCLUDED_SDR_HOLOGRAPHY_AVERAGE_FF_H */
