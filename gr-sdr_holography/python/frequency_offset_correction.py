#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright 2022 gr-sdr_holography author.
#
# SPDX-License-Identifier: GPL-3.0-or-later
#


# import numpy
from gnuradio import gr
import numpy as np
from scipy.signal import get_window
from scipy.fftpack import fft, fftshift


class frequency_offset_correction(gr.sync_block):
    """
    docstring for block frequency_offset_correction
    """
    def __init__(self, num_items, win, freq, samp_rate):
        self.win = win
        gr.sync_block.__init__(self,
            name="frequency_offset_correction",
            in_sig=[(np.complex64, len(self.win))],
            out_sig=[(np.complex64, len(self.win))])
        self.num_items = num_items
        self.freq = freq
        self.samp_rate = samp_rate

    def peak_detection(self, in_sig):
        """
        Determines the maximum peak location of the given input signal samples.
        The peak is given by p = mX[k_o] when mX[k_o - 1] < mX[k_o] > mX[k_o + 1].
        Returns this as a tuple made up of the first element being the frequency
        bin of the peak value, second element of the tuple as magnitude of the peak,
        third element as interpolated peak bin and the fourth last element as the
        magnitude of the ipolated peak.

        Parameters
        ----------
        :param in_sig: numpy array

        Returns
        -------
         : Float
            Value of the interpolated frequency bin
        """

        # Select beginning of signal to be same as window size
        wind_size = len(self.win)
        time = 0
        in_sig = in_sig[int(time * self.samp_rate):int(time * self.samp_rate) + wind_size]

        # Apply a window to the input signal
        windowed_sig = in_sig * self.win

        # Compute DFT using FFT and put DC at center using fftshift functiom
        in_sig_fft = fft(windowed_sig, self.num_items)
        in_sig_fft = in_sig_fft[0]
        in_sig_fftshit = fftshift(in_sig_fft)

        # The parabolic interpolation only works correctly with the peak of
        # the main lobe in dB scale not linear
        in_sig_abs = abs(in_sig_fftshit)
        # replace zeros with epsilon to handle log
        in_sig_abs[in_sig_abs < np.finfo(float).eps] = np.finfo(float).eps
        in_sig_mag = 20 * np.log10(in_sig_abs)

        # Extract local maxima
        peak_mag = np.amax(in_sig_mag)
        peak_loc = np.where(in_sig_mag == peak_mag)[0][0]

        # -------------------------------------------------

        left_bin_mag = in_sig_mag[peak_loc-1]          # magnitude of bin at the left
        peak_bin_mag = in_sig_mag[peak_loc]             # magnitude of peak bin
        right_bin_mag = in_sig_mag[peak_loc+1]          # magnitude of bin at the right

        # Search for the peak bin in the range of frequency bins
        f = np.arange(-self.num_items / 2, self.num_items / 2) * self.samp_rate / self.num_items
        peak_loc = f[peak_loc]

        # center of parabola
        interpolated_peakloc = peak_loc + 0.5*(left_bin_mag-right_bin_mag)/(left_bin_mag-2*peak_bin_mag+right_bin_mag)

        # magnitude of peaks
        interpolated_mag = peak_bin_mag - 0.25 * (left_bin_mag-right_bin_mag) * (interpolated_peakloc-peak_loc)

        return interpolated_peakloc


    def freq_shifting(self, freq_shift, y):
        """
        Use the given interpolated peak value to shift the spectrum of the input array
        to the center of the spectrum.

        Parameters
        ----------
            Frequency shift value to be commanded
        :param freq_shift:float
            Peak magnitude value
        :param y:numpy array
            Values of the input signal

        Returns
        -------
        corrected_mX : ndarray
            The shifted array.
        """
        freq_offset = freq_shift - self.freq
        corrected_y = y * np.exp(-1j * 2 * np.pi * freq_offset * np.arange(len(y)) / self.samp_rate)

        return corrected_y


    def work(self, input_items, output_items):
        in0 = input_items[0]
        out = output_items[0]

        # signal processing here
        # We first use peak detection and parabolic interpolation to find the location of the 
        # center frequency
        peak_bin_location = self.peak_detection(in0)

        # Calculate offset and multiply input signal sample by sample
        for i in range(0, len(in0)):
            out[i] = self.freq_shifting(self.freq, in0[i])


        return len(output_items[0])
