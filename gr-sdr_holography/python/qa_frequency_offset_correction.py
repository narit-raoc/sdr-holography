#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright 2022 gr-sdr_holography author.
#
# SPDX-License-Identifier: GPL-3.0-or-later
#

from gnuradio import gr, gr_unittest
from gnuradio import blocks
from frequency_offset_correction import frequency_offset_correction
import numpy as np
from scipy.signal import get_window
# import frequency_offset_correction as fo
from scipy.fftpack import fft, fftshift
import matplotlib.pyplot as plt

class qa_frequency_offset_correction(gr_unittest.TestCase):

    def setUp(self):
        self.tb = gr.top_block()

    def tearDown(self):
        self.tb = None

    # def test_instance(self):
    #     # FIXME: Test will fail until you pass sensible arguments to the constructor
    #     # instance = frequency_offset_correction()
    #     freqoffblock = frequency_offset_correction(fft_size, w, freq, fs)

    def test_001_descriptive_test_name(self):
        # set up fg
        length_signal = 6
        fs = 2e4
        freq = 20
        fft_size = 512
        no_samples = int(length_signal * fs)
        t0 = np.arange(0, no_samples) / fs
        signal = np.exp(1j * (2 * np.pi * freq * t0))
        y = signal * np.exp(-1j * 2 * np.pi * (-1000 / fs) * np.arange(len(signal)))
        X, mX, _ = dft_comp(fs, y, fft_size)
        src = blocks.vector_source_c(y)
        w = get_window('hamming',  512)
        s2v = blocks.stream_to_vector(gr.sizeof_gr_complex, len(w))
        freqoffblock = frequency_offset_correction(fft_size, w, freq, fs)
        dest = blocks.vector_sink_c()
        v2s = blocks.vector_to_stream(gr.sizeof_gr_complex, len(w))
        self.tb.connect(src, s2v, freqoffblock, v2s, dest)
        self.tb.run()
        result_data = dest.data()
        Y, mY, _ = dft_comp(fs, result_data, fft_size)

        plt.figure(1)
        plt.plot(np.arange(-fft_size / 2, fft_size / 2) * (fs / fft_size), mX)

        # plt.figure(2)
        # plt.plot(np.arange(-fft_size / 2, fft_size / 2) * (fs / fft_size), mY)
        plt.show()
        #check data

def dft_comp(fs, in_sig, fft_size, w=get_window('hamming', 512)):
    """
    Computes the dft of the input signal.
    Apply the chosen window to the input signal and computes
    dft and returns a tuple of the computed dft and magnitude
    and phase spectrum.

    Parameters
    ----------
    :param fs:
    :param in_sig: numpy array
        Input signal.
    :param w: numpy array of the given window size
    :param fft_size: int
                FFT size of the output spectrum

    Returns
    -------
    in_sig_fftshit, in_sig_mag, in_sig_phase : tuple
        The first element in the tupple is the fft of the input signal,
        The second element of the tuple is the magnitude spectrum (dB) and
        The third element is the phase spectrum of the input signal
    """

    # Select beginning of signal to be same as window size
    wind_size = len(w)
    time = 0
    in_sig = in_sig[int(time * fs):int(time * fs) + wind_size]

    windowed_sig = in_sig * w
    in_sig_fft = fft(windowed_sig, fft_size)
    in_sig_fftshit = fftshift(in_sig_fft)
    # compute the absolute value of positive frequencies
    in_sig_abs = abs(in_sig_fftshit)
    # replace zeros with epsilon to handle log
    in_sig_abs[in_sig_abs < np.finfo(float).eps] = np.finfo(float).eps

    # magnitude spectrum
    in_sig_mag = 20 * np.log10(in_sig_abs)
    # phase spectrum
    in_sig_phase = np.angle(in_sig_fft)

    return in_sig_fftshit, in_sig_mag, in_sig_phase

if __name__ == '__main__':
    gr_unittest.run(qa_frequency_offset_correction)
