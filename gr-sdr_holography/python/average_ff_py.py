#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright 2022 gr-sdr_holography author.
#
# SPDX-License-Identifier: GPL-3.0-or-later
#


import numpy as np
from gnuradio import gr

class average_ff_py(gr.decim_block):
    """
    docstring for block average_ff_py
    """
    def __init__(self, decim):
        gr.decim_block.__init__(self,
            name="average_ff_py",
            in_sig=[np.float32],
            out_sig=[np.float32], decim=int(decim))

        self.decim = int(decim)

    # def forecast(self, nouput_items, ninput_items):
    #     """
    #     Indicate the number of inputs required to produce one output
    #     """
    #     for i in range(len(nouput_items)):
    #         ninput_items[i] = nouput_items[i] * self.decim
    #     return ninput_items

    def work(self, input_items, output_items):
        in0 = input_items[0]
        out = output_items[0]

        # Average fs input samples to produce one value
        # Reduces the input high sampling rate 
        in0 = in0[:len(in0)]

        # self.avg_val = np.sum(in0) / self.decim
        out[:] = np.sum(in0) / self.decim
        # print(f'out: {out}')


        return len(output_items[0])


