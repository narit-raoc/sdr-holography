#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright 2022 gr-sdr_holography author.
#
# SPDX-License-Identifier: GPL-3.0-or-later
#

from gnuradio import gr, gr_unittest
from gnuradio import blocks
from print_metadata import print_metadata
import pmt

class qa_print_metadata(gr_unittest.TestCase):

    def setUp(self):
        self.tb = gr.top_block()

    def tearDown(self):
        self.tb = None

    def test_instance(self):
        # FIXME: Test will fail until you pass sensible arguments to the constructor
        instance = print_metadata()

    def test_001_descriptive_test_name(self):
        # set up fg
        self.tb.run()
        # check data

    def test_002_tags_plus_data(self):

        packet_len = 6
        lenx = 6
        tag1 = gr.tag_t()
        tag1.offset = 0
        tag1.key = pmt.string_to_symbol('spam')
        tag1.value = pmt.from_long(23)
        tag2 = gr.tag_t()
        tag2.offset = 2  # Must be < packet_len
        tag2.key = pmt.string_to_symbol('eggs')
        tag2.value = pmt.from_long(42)
        src0 = blocks.vector_source_f(list(range(0, lenx, 6)), tags=(tag1, tag2))
        # src0 = blocks.vector_source_f(list(range(0, lenx, 6)))
        src1 = blocks.vector_source_f(list(range(1, lenx, 6)))
        src2 = blocks.vector_source_f(list(range(2, lenx, 6)))
        src3 = blocks.vector_source_f(list(range(3, lenx, 6)))
        src4 = blocks.vector_source_f(list(range(4, lenx, 6)))
        src5 = blocks.vector_source_f(list(range(4, lenx, 6)))
        ip = blocks.interleave(gr.sizeof_float*1, 1)

        s2ts = blocks.stream_to_tagged_stream(
            gr.sizeof_float,
            vlen=1,
            packet_len=packet_len,
            len_tag_key="packet_len")
        ts2pdu = blocks.tagged_stream_to_pdu(blocks.float_t, "packet_len")
        instance = print_metadata()

        self.tb.connect(src0, (ip, 0))
        self.tb.connect(src1, (ip, 1))
        self.tb.connect(src2, (ip, 2))
        self.tb.connect(src3, (ip, 3))
        self.tb.connect(src4, (ip, 4))
        self.tb.connect(src5, (ip, 5))
        self.tb.connect(ip, s2ts, ts2pdu)
        self.tb.msg_connect(ts2pdu, "pdus", instance, "print")
        # self.tb.run()
        self.tb.start()
        self.tb.wait()
        # result_msg = instance.get_message(0)
        # pmt.print(result_msg)
        # expected_result = tuple(range(lenx))
        # result_data = dst.data()
        # self.assertFloatTuplesAlmostEqual(expected_result, result_data)
        # vector = pmt.f32vector_elements(pmt.cdr(result_msg))
        # self.assertFloatTuplesAlmostEqual(tuple(vector), src_data)


if __name__ == '__main__':
    gr_unittest.run(qa_print_metadata)
