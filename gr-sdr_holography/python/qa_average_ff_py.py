#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright 2022 gr-sdr_holography author.
#
# SPDX-License-Identifier: GPL-3.0-or-later
#

import numpy as np
from gnuradio import gr, gr_unittest
from gnuradio import blocks
from average_ff_py import average_ff_py

class qa_average_ff_py(gr_unittest.TestCase):

    def setUp(self):
        self.tb = gr.top_block()

    def tearDown(self):
        self.tb = None

    # def test_instance(self):
    #     # FIXME: Test will fail until you pass sensible arguments to the constructor
    #     instance = average_ff_py()

    def test_001_descriptive_test_name(self):
        # set up fg
        decim = 16

        # create a set of vectors
        src_data = np.arange(0, 16)
        expected_data = 7.5
        src = blocks.vector_source_f(src_data)
        # s2v = blocks.stream_to_vector(gr.sizeof_float, decim)
        
        avgblock = average_ff_py(decim)


        # v2s = blocks.vector_to_stream(gr.sizeof_float, decim)
        snk = blocks.vector_sink_f()

        # self.tb.connect(src, s2v)
        self.tb.connect(src, avgblock)
        self.tb.connect(avgblock, snk)
   

        self.tb.run()

        # check data
        outdata = snk.data()

        print(f'data: {outdata}')

        # check data
        # self.assertAlmostEqual(expected_data, outdata)


if __name__ == '__main__':
    gr_unittest.run(qa_average_ff_py)
