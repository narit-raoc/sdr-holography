#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright 2022 gr-sdr_holography author.
#
# SPDX-License-Identifier: GPL-3.0-or-later
#


import numpy
from gnuradio import gr
import pmt
import time

class print_metadata(gr.basic_block):
    """
    This block subscribes to PDU messages which are essentially a tagged stream with 
    a specific length. The message is a PMT type object which consists of two parts;
    1. car - which is made of the meta data 
    2. cdr - gives the payload data
    """
    def __init__(self):
        gr.basic_block.__init__(self,
            name="print_metadata",
            in_sig=None,
            out_sig=None)

        # registering the message input port
        self.message_port_register_in(pmt.intern('print'))
        # registering the message handler callback function
        self.set_msg_handler(pmt.intern('print'), self.handle_msg)
        self.packet_counter = 0


    def handle_msg(self, msg):
        meta = pmt.car(msg)
        payload = pmt.cdr(msg)
        print(f'Packet number: {self.packet_counter}')
        payload = pmt.to_python(payload)
        keys = ['RF0 Mag', 'RF0 Phase', 'RF1 Mag', 'RF1 Phase', 'Mag Diff', 'Phase Diff']
        payload_dict = dict(zip(keys, list(payload)))
        if pmt.to_python(meta) == None:
            print(payload_dict)
        else:
            print(f'Metadata: {pmt.to_python(meta)}')
            print(payload_dict)
            
        self.packet_counter += 1


        


