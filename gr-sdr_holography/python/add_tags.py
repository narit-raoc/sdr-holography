#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright 2022 gr-sdr_holography author.
#
# SPDX-License-Identifier: GPL-3.0-or-later
#


import numpy
from gnuradio import gr
from datetime import datetime, timedelta
import pmt

class add_tags(gr.sync_block):
    """
    docstring for block add_tags
    """
    def __init__(self, rx_rate, rx_freq):
        gr.sync_block.__init__(self,
            name="add_tags",
            in_sig=[numpy.float32],
            out_sig=[numpy.float32])

        self.rx_rate = rx_rate
        self.rx_freq = rx_freq
        


    def work(self, input_items, output_items):
        in0 = input_items[0]

        t0 = datetime.now()
        for indx, sample in enumerate(in0):
            if indx == 0:
                key0 = pmt.intern('rx_rate')
                value0 = pmt.to_pmt(self.rx_rate)
                self.add_item_tag(0, self.nitems_written(0) + indx, key0, value0)
                key1 = pmt.intern('rx_freq')
                value1 = pmt.to_pmt(self.rx_freq)
                self.add_item_tag(0, self.nitems_written(0) + indx, key1, value1)

            key = pmt.intern("rx_time")
            value = pmt.to_pmt(t0)
            self.add_item_tag(0, self.nitems_written(0) + indx, key, value)
            t0 = t0 + timedelta(seconds=1)
        out = output_items[0]
        # <+signal processing here+>
        out[:] = in0
        return len(output_items[0])
