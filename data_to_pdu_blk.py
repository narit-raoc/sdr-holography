# -*- coding: utf-8 -*-

#
# SPDX-License-Identifier: GPL-3.0
#
# GNU Radio Python Flow Graph
# Title: Data Stream to PDU
# Author: bright
# GNU Radio version: v3.9.5.0-62-gf520c346

from gnuradio import blocks
from gnuradio import gr
from gnuradio.filter import firdes
from gnuradio.fft import window
import sys
import signal
import sdr_holography







class data_to_pdu_blk(gr.hier_block2):
    def __init__(self, decim=int(150e3), samp_rate=150e3):
        gr.hier_block2.__init__(
            self, "Data Stream to PDU",
                gr.io_signature.makev(6, 6, [gr.sizeof_float*1, gr.sizeof_float*1, gr.sizeof_float*1, gr.sizeof_float*1, gr.sizeof_float*1, gr.sizeof_float*1]),
                gr.io_signature(0, 0, 0),
        )
        self.message_port_register_hier_out("pdus")

        ##################################################
        # Parameters
        ##################################################
        self.decim = decim
        self.samp_rate = samp_rate

        ##################################################
        # Blocks
        ##################################################
        self.sdr_holography_average_ff_py_4 = sdr_holography.average_ff_py(decim)
        self.sdr_holography_average_ff_py_3 = sdr_holography.average_ff_py(decim)
        self.sdr_holography_average_ff_py_2 = sdr_holography.average_ff_py(decim)
        self.sdr_holography_average_ff_py_1_0 = sdr_holography.average_ff_py(decim)
        self.sdr_holography_average_ff_py_1 = sdr_holography.average_ff_py(decim)
        self.sdr_holography_average_ff_py_0 = sdr_holography.average_ff_py(decim)
        self.blocks_tagged_stream_to_pdu_0 = blocks.tagged_stream_to_pdu(blocks.float_t, 'packet_len')
        self.blocks_stream_to_tagged_stream_0 = blocks.stream_to_tagged_stream(gr.sizeof_float, 1, 6, "packet_len")
        self.blocks_interleave_0 = blocks.interleave(gr.sizeof_float*1, 1)


        ##################################################
        # Connections
        ##################################################
        self.msg_connect((self.blocks_tagged_stream_to_pdu_0, 'pdus'), (self, 'pdus'))
        self.connect((self.blocks_interleave_0, 0), (self.blocks_stream_to_tagged_stream_0, 0))
        self.connect((self.blocks_stream_to_tagged_stream_0, 0), (self.blocks_tagged_stream_to_pdu_0, 0))
        self.connect((self, 0), (self.sdr_holography_average_ff_py_0, 0))
        self.connect((self, 1), (self.sdr_holography_average_ff_py_4, 0))
        self.connect((self, 2), (self.sdr_holography_average_ff_py_3, 0))
        self.connect((self, 3), (self.sdr_holography_average_ff_py_2, 0))
        self.connect((self, 4), (self.sdr_holography_average_ff_py_1, 0))
        self.connect((self, 5), (self.sdr_holography_average_ff_py_1_0, 0))
        self.connect((self.sdr_holography_average_ff_py_0, 0), (self.blocks_interleave_0, 0))
        self.connect((self.sdr_holography_average_ff_py_1, 0), (self.blocks_interleave_0, 4))
        self.connect((self.sdr_holography_average_ff_py_1_0, 0), (self.blocks_interleave_0, 5))
        self.connect((self.sdr_holography_average_ff_py_2, 0), (self.blocks_interleave_0, 3))
        self.connect((self.sdr_holography_average_ff_py_3, 0), (self.blocks_interleave_0, 1))
        self.connect((self.sdr_holography_average_ff_py_4, 0), (self.blocks_interleave_0, 2))


    def get_decim(self):
        return self.decim

    def set_decim(self, decim):
        self.decim = decim

    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate

