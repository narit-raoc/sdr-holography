import sys
import zmq
import numpy
import pmt
import socket

# digital.constellation_calcdist

# socket to talk to the server in GNU radio
context = zmq.Context()
socket = context.socket(zmq.SUB)
socket.connect('tcp://127.0.0.1:150245')
socket.setsockopt_string(zmq.SUBSCRIBE, '')
print('running')

while True:
    string = socket.recv()
    # print(string)
    deserialize_str = pmt.deserialize_str(string)
    print(f'Received: {deserialize_str}')
    # python_obj = pmt.to_python(string)
    # meta = pmt.car(deserialize_str)
    # payload = pmt.cdr(deserialize_str)
    # # print(pmt.is_pdu(deserialize_str))
    # pmt.print(meta)
    # pmt.print(payload)
    # # print(python_obj)


