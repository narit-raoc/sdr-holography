#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#
# SPDX-License-Identifier: GPL-3.0
#
# GNU Radio Python Flow Graph
# Title: Program running on the n310
# Author: bright
# GNU Radio version: v3.9.5.0-62-gf520c346

from gnuradio import blocks
from gnuradio import gr
from gnuradio.filter import firdes
from gnuradio.fft import window
import sys
import signal
from argparse import ArgumentParser
from gnuradio.eng_arg import eng_float, intx
from gnuradio import eng_notation
from gnuradio import uhd
import time
from gnuradio import zeromq
from xmlrpc.server import SimpleXMLRPCServer
import threading
import howto
from scipy.signal import get_window




class holorx_server(gr.top_block):

    def __init__(self, freq_rftune=70e6, gain_rx0=0, gain_rx2=0):
        gr.top_block.__init__(self, "Program running on the n310", catch_exceptions=True)

        ##################################################
        # Parameters
        ##################################################
        self.freq_rftune = freq_rftune
        self.gain_rx0 = gain_rx0
        self.gain_rx2 = gain_rx2

        ##################################################
        # Variables
        ##################################################
        self.samp_rate = samp_rate = 125000
        self.ch_tb = ch_tb = 580
        self.ch_bw = ch_bw = 500
        self.atten = atten = 60
        self.taps = taps = firdes.low_pass_2(1, samp_rate, ch_bw, ch_tb, atten)
        self.server_port = server_port = 30000
        self.server_address = server_address = "127.0.0.1"
        self.reference_signal = reference_signal = 70e6
        self.fft_size = fft_size = 1024
        self.channels = channels = 125
        self.channel = channel = 0
        self.address = address = "3180B03"

        ##################################################
        # Blocks
        ##################################################
        self.zeromq_push_sink_1 = zeromq.push_sink(gr.sizeof_gr_complex, 1, 'tcp://127.0.0.1:8800', 100, False, -1)
        self.zeromq_push_sink_0 = zeromq.push_sink(gr.sizeof_gr_complex, 1, 'tcp://127.0.0.1:8801', 100, False, -1)
        self.xmlrpc_server_1 = SimpleXMLRPCServer((server_address, server_port), allow_none=True)
        self.xmlrpc_server_1.register_instance(self)
        self.xmlrpc_server_1_thread = threading.Thread(target=self.xmlrpc_server_1.serve_forever)
        self.xmlrpc_server_1_thread.daemon = True
        self.xmlrpc_server_1_thread.start()
        self.uhd_usrp_source_0 = uhd.usrp_source(
            ",".join(("", "")),
            uhd.stream_args(
                cpu_format="fc32",
                args='',
                channels=list(range(0,2)),
            ),
        )
        self.uhd_usrp_source_0.set_subdev_spec("A:0 B:0", 0)
        self.uhd_usrp_source_0.set_samp_rate(samp_rate)
        self.uhd_usrp_source_0.set_time_now(uhd.time_spec(time.time()), uhd.ALL_MBOARDS)

        self.uhd_usrp_source_0.set_center_freq(uhd.tune_request(freq_rftune, 100e3), 0)
        self.uhd_usrp_source_0.set_antenna('RX2', 0)
        self.uhd_usrp_source_0.set_gain(gain_rx0, 0)

        self.uhd_usrp_source_0.set_center_freq(uhd.tune_request(reference_signal, 100e3), 1)
        self.uhd_usrp_source_0.set_antenna('RX2', 1)
        self.uhd_usrp_source_0.set_gain(gain_rx2, 1)
        self.howto_frequency_offset_correction_1 = howto.frequency_offset_correction(1024, get_window('hamming', fft_size), 0, samp_rate/channels)
        self.blocks_vector_to_stream_1 = blocks.vector_to_stream(gr.sizeof_gr_complex*1, fft_size)
        self.blocks_vector_to_stream_0 = blocks.vector_to_stream(gr.sizeof_gr_complex*1, fft_size)
        self.blocks_stream_to_vector_1 = blocks.stream_to_vector(gr.sizeof_gr_complex*1, fft_size)
        self.blocks_stream_to_vector_0 = blocks.stream_to_vector(gr.sizeof_gr_complex*1, fft_size)


        ##################################################
        # Connections
        ##################################################
        self.connect((self.blocks_stream_to_vector_0, 0), (self.howto_frequency_offset_correction_1, 0))
        self.connect((self.blocks_stream_to_vector_1, 0), (self.blocks_vector_to_stream_1, 0))
        self.connect((self.blocks_vector_to_stream_0, 0), (self.zeromq_push_sink_1, 0))
        self.connect((self.blocks_vector_to_stream_1, 0), (self.zeromq_push_sink_0, 0))
        self.connect((self.howto_frequency_offset_correction_1, 0), (self.blocks_vector_to_stream_0, 0))
        self.connect((self.uhd_usrp_source_0, 0), (self.blocks_stream_to_vector_0, 0))
        self.connect((self.uhd_usrp_source_0, 1), (self.blocks_stream_to_vector_1, 0))


    def get_freq_rftune(self):
        return self.freq_rftune

    def set_freq_rftune(self, freq_rftune):
        self.freq_rftune = freq_rftune
        self.uhd_usrp_source_0.set_center_freq(uhd.tune_request(self.freq_rftune, 100e3), 0)

    def get_gain_rx0(self):
        return self.gain_rx0

    def set_gain_rx0(self, gain_rx0):
        self.gain_rx0 = gain_rx0
        self.uhd_usrp_source_0.set_gain(self.gain_rx0, 0)

    def get_gain_rx2(self):
        return self.gain_rx2

    def set_gain_rx2(self, gain_rx2):
        self.gain_rx2 = gain_rx2
        self.uhd_usrp_source_0.set_gain(self.gain_rx2, 1)

    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate
        self.set_taps(firdes.low_pass_2(1, self.samp_rate, self.ch_bw, self.ch_tb, self.atten))
        self.uhd_usrp_source_0.set_samp_rate(self.samp_rate)

    def get_ch_tb(self):
        return self.ch_tb

    def set_ch_tb(self, ch_tb):
        self.ch_tb = ch_tb
        self.set_taps(firdes.low_pass_2(1, self.samp_rate, self.ch_bw, self.ch_tb, self.atten))

    def get_ch_bw(self):
        return self.ch_bw

    def set_ch_bw(self, ch_bw):
        self.ch_bw = ch_bw
        self.set_taps(firdes.low_pass_2(1, self.samp_rate, self.ch_bw, self.ch_tb, self.atten))

    def get_atten(self):
        return self.atten

    def set_atten(self, atten):
        self.atten = atten
        self.set_taps(firdes.low_pass_2(1, self.samp_rate, self.ch_bw, self.ch_tb, self.atten))

    def get_taps(self):
        return self.taps

    def set_taps(self, taps):
        self.taps = taps

    def get_server_port(self):
        return self.server_port

    def set_server_port(self, server_port):
        self.server_port = server_port

    def get_server_address(self):
        return self.server_address

    def set_server_address(self, server_address):
        self.server_address = server_address

    def get_reference_signal(self):
        return self.reference_signal

    def set_reference_signal(self, reference_signal):
        self.reference_signal = reference_signal
        self.uhd_usrp_source_0.set_center_freq(uhd.tune_request(self.reference_signal, 100e3), 1)

    def get_fft_size(self):
        return self.fft_size

    def set_fft_size(self, fft_size):
        self.fft_size = fft_size

    def get_channels(self):
        return self.channels

    def set_channels(self, channels):
        self.channels = channels

    def get_channel(self):
        return self.channel

    def set_channel(self, channel):
        self.channel = channel

    def get_address(self):
        return self.address

    def set_address(self, address):
        self.address = address



def argument_parser():
    parser = ArgumentParser()
    parser.add_argument(
        "--freq-rftune", dest="freq_rftune", type=eng_float, default=eng_notation.num_to_str(float(70e6)),
        help="Set freq_rftune [default=%(default)r]")
    parser.add_argument(
        "--gain-rx0", dest="gain_rx0", type=eng_float, default=eng_notation.num_to_str(float(0)),
        help="Set gain_rx0 [default=%(default)r]")
    parser.add_argument(
        "--gain-rx2", dest="gain_rx2", type=eng_float, default=eng_notation.num_to_str(float(0)),
        help="Set gain_rx2 [default=%(default)r]")
    return parser


def main(top_block_cls=holorx_server, options=None):
    if options is None:
        options = argument_parser().parse_args()
    tb = top_block_cls(freq_rftune=options.freq_rftune, gain_rx0=options.gain_rx0, gain_rx2=options.gain_rx2)

    def sig_handler(sig=None, frame=None):
        tb.stop()
        tb.wait()

        sys.exit(0)

    signal.signal(signal.SIGINT, sig_handler)
    signal.signal(signal.SIGTERM, sig_handler)

    tb.start()

    try:
        input('Press Enter to quit: ')
    except EOFError:
        pass
    tb.stop()
    tb.wait()


if __name__ == '__main__':
    main()
