"""
Data analysis program
    - mean(dc)
    - Standard deviation (AC/fluctuations from mean)
    - variance (power of signal)
    - Histograms
    - Probability mass functions (pmf)
"""

import pmt
from gnuradio.blocks import parse_file_metadata
import numpy as np
from scipy.signal import get_window
from scipy.fftpack import fft, fftshift
import matplotlib.pyplot as plt


def read_metadata_files(filename, max_dat_segmt_to_read, print_output=True):
    fh = open(filename, "rb")
    samples = np.array([])
    for ii in range(max_dat_segmt_to_read):
        header_str = fh.read(parse_file_metadata.HEADER_LENGTH)
        header = pmt.deserialize_str(header_str)

        print(f'\n==Data segment {ii} ==')

        header_info = parse_file_metadata.parse_header(header, print_output)
        if header_info["extra_len"] > 0:
            extra_str = fh.read(header_info["extra_len"])
            if len(extra_str) != 0:
                extra = pmt.deserialize_str(extra_str)
                extra_info = parse_file_metadata.parse_extra_dict(extra, header_info, print_output)

        data = np.fromfile(file=fh, dtype=np.float32, count=2*int(header_info["nitems"]), sep='', offset=0)
        print(f'{len(data)} data elements read')
        samples = np.append(samples, data)
    fh.close()
    return samples


file_chan0 = './Results Analysis/Data/holo_metadata_RF0.bin'
file_chan1 = './Results Analysis/Data/holo_metadata_RF1.bin'

max_data_segments_to_read = 5

# Read samples for channel 0
samples_chan0 = read_metadata_files(file_chan0, max_data_segments_to_read)
samples_chan0 = samples_chan0[::2] + 1j*samples_chan0[1::2]

# Read samples for channel 1
samples_chan1 = read_metadata_files(file_chan1, max_data_segments_to_read)
samples_chan1 = samples_chan1[::2] + 1j*samples_chan1[1::2]

# Calculate magnitude for each channel
ch0_mag = 20 * np.log10(np.abs(samples_chan0))
ch1_mag = 20 * np.log10(np.abs(samples_chan1))

mag_diff = ch0_mag - ch1_mag
stand_dev_mag = np.std(mag_diff)
print(f'Standard Deviation of Difference in Amplitude: {stand_dev_mag}')

# Claculate phase for channel
ch0_phs = np.angle(samples_chan0)
ch1_phs = np.angle(samples_chan1)

phase_diff = np.unwrap((ch0_phs - ch1_phs))
stand_dev_phs = np.std(phase_diff)
print(f'Standard Deviation of Difference in Phase: {stand_dev_phs}')


plt.figure(1)
plt.plot(mag_diff)
plt.xlabel('Time(s)')
plt.ylabel('Amplitude Difference (dB)')

plt.figure(2)
plt.plot(phase_diff)
plt.xlabel('Time(s)')
plt.ylabel('Phase Difference (rad)')

# samples = np.fromfile('holodata.iq', np.complex64)
# print(len(samples))
#
# fs = 125e3
# fft_size = 1024
#
# delta = fs / fft_size
# f = np.arange(-fft_size / 2, fft_size / 2) * delta
#
# window_size = 501
# window = get_window('blackman', window_size)
#
# t = 400000
# windowed_signal = samples[t:t + window_size] * window
#
# mX = fft(windowed_signal, fft_size)
# mX = fftshift(mX)
#
# mX_abs = abs(mX)
#
# mX_dB = 20 * np.log10(mX_abs)
#
# plt.figure(1)
# plt.plot(f, mX_dB)

plt.show()
