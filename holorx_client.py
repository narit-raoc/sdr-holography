#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#
# SPDX-License-Identifier: GPL-3.0
#
# GNU Radio Python Flow Graph
# Title: Program running on the n310
# Author: bright
# GNU Radio version: v3.9.5.0-62-gf520c346

from distutils.version import StrictVersion

if __name__ == '__main__':
    import ctypes
    import sys
    if sys.platform.startswith('linux'):
        try:
            x11 = ctypes.cdll.LoadLibrary('libX11.so')
            x11.XInitThreads()
        except:
            print("Warning: failed to XInitThreads()")

import os
import sys
sys.path.append(os.environ.get('GRC_HIER_PATH', os.path.expanduser('~/.grc_gnuradio')))

from PyQt5 import Qt
from gnuradio import qtgui
from gnuradio.filter import firdes
import sip
from fft_bin_select import fft_bin_select  # grc-generated hier_block
from gnuradio import blocks
from gnuradio import gr
from gnuradio.fft import window
import signal
from argparse import ArgumentParser
from gnuradio.eng_arg import eng_float, intx
from gnuradio import eng_notation
from gnuradio import zeromq
from gnuradio.qtgui import Range, RangeWidget
from PyQt5 import QtCore
from xmlrpc.client import ServerProxy
import numpy as np
import pmt



from gnuradio import qtgui

class holorx_client(gr.top_block, Qt.QWidget):

    def __init__(self):
        gr.top_block.__init__(self, "Program running on the n310", catch_exceptions=True)
        Qt.QWidget.__init__(self)
        self.setWindowTitle("Program running on the n310")
        qtgui.util.check_set_qss()
        try:
            self.setWindowIcon(Qt.QIcon.fromTheme('gnuradio-grc'))
        except:
            pass
        self.top_scroll_layout = Qt.QVBoxLayout()
        self.setLayout(self.top_scroll_layout)
        self.top_scroll = Qt.QScrollArea()
        self.top_scroll.setFrameStyle(Qt.QFrame.NoFrame)
        self.top_scroll_layout.addWidget(self.top_scroll)
        self.top_scroll.setWidgetResizable(True)
        self.top_widget = Qt.QWidget()
        self.top_scroll.setWidget(self.top_widget)
        self.top_layout = Qt.QVBoxLayout(self.top_widget)
        self.top_grid_layout = Qt.QGridLayout()
        self.top_layout.addLayout(self.top_grid_layout)

        self.settings = Qt.QSettings("GNU Radio", "holorx_client")

        try:
            if StrictVersion(Qt.qVersion()) < StrictVersion("5.0.0"):
                self.restoreGeometry(self.settings.value("geometry").toByteArray())
            else:
                self.restoreGeometry(self.settings.value("geometry"))
        except:
            pass

        ##################################################
        # Variables
        ##################################################
        self.samp_rate = samp_rate = 1e3
        self.gain_rx2 = gain_rx2 = 0
        self.gain_rx0 = gain_rx0 = 0
        self.freq_rftune = freq_rftune = 70e6
        self.client_address = client_address = "127.0.0.1"

        ##################################################
        # Blocks
        ##################################################
        self.zeromq_pull_source_1 = zeromq.pull_source(gr.sizeof_gr_complex, 1, 'tcp://127.0.0.1:8801', 100, False, -1)
        self.zeromq_pull_source_0 = zeromq.pull_source(gr.sizeof_gr_complex, 1, 'tcp://127.0.0.1:8800', 100, False, -1)
        self.xmlrpc_client_2 = ServerProxy('http://'+client_address+':30000')
        self.xmlrpc_client_1 = ServerProxy('http://'+client_address+':30000')
        self.xmlrpc_client_0 = ServerProxy('http://'+client_address+':30000')
        self.qtgui_number_sink_1 = qtgui.number_sink(
            gr.sizeof_float,
            0,
            qtgui.NUM_GRAPH_NONE,
            2,
            None # parent
        )
        self.qtgui_number_sink_1.set_update_time(0.10)
        self.qtgui_number_sink_1.set_title('Magnitude (dB)')

        labels = ['', '', '', '', '',
            '', '', '', '', '']
        units = ['', '', '', '', '',
            '', '', '', '', '']
        colors = [("black", "black"), ("black", "black"), ("black", "black"), ("black", "black"), ("black", "black"),
            ("black", "black"), ("black", "black"), ("black", "black"), ("black", "black"), ("black", "black")]
        factor = [1, 1, 1, 1, 1,
            1, 1, 1, 1, 1]

        for i in range(2):
            self.qtgui_number_sink_1.set_min(i, -1)
            self.qtgui_number_sink_1.set_max(i, 1)
            self.qtgui_number_sink_1.set_color(i, colors[i][0], colors[i][1])
            if len(labels[i]) == 0:
                self.qtgui_number_sink_1.set_label(i, "Data {0}".format(i))
            else:
                self.qtgui_number_sink_1.set_label(i, labels[i])
            self.qtgui_number_sink_1.set_unit(i, units[i])
            self.qtgui_number_sink_1.set_factor(i, factor[i])

        self.qtgui_number_sink_1.enable_autoscale(False)
        self._qtgui_number_sink_1_win = sip.wrapinstance(self.qtgui_number_sink_1.qwidget(), Qt.QWidget)
        self.top_layout.addWidget(self._qtgui_number_sink_1_win)
        self.qtgui_number_sink_0 = qtgui.number_sink(
            gr.sizeof_float,
            0,
            qtgui.NUM_GRAPH_NONE,
            2,
            None # parent
        )
        self.qtgui_number_sink_0.set_update_time(0.10)
        self.qtgui_number_sink_0.set_title('Difference RF0 - RF2')

        labels = ['', '', '', '', '',
            '', '', '', '', '']
        units = ['', '', '', '', '',
            '', '', '', '', '']
        colors = [("black", "black"), ("black", "black"), ("black", "black"), ("black", "black"), ("black", "black"),
            ("black", "black"), ("black", "black"), ("black", "black"), ("black", "black"), ("black", "black")]
        factor = [1, 1, 1, 1, 1,
            1, 1, 1, 1, 1]

        for i in range(2):
            self.qtgui_number_sink_0.set_min(i, -1)
            self.qtgui_number_sink_0.set_max(i, 1)
            self.qtgui_number_sink_0.set_color(i, colors[i][0], colors[i][1])
            if len(labels[i]) == 0:
                self.qtgui_number_sink_0.set_label(i, "Data {0}".format(i))
            else:
                self.qtgui_number_sink_0.set_label(i, labels[i])
            self.qtgui_number_sink_0.set_unit(i, units[i])
            self.qtgui_number_sink_0.set_factor(i, factor[i])

        self.qtgui_number_sink_0.enable_autoscale(False)
        self._qtgui_number_sink_0_win = sip.wrapinstance(self.qtgui_number_sink_0.qwidget(), Qt.QWidget)
        self.top_layout.addWidget(self._qtgui_number_sink_0_win)
        self.qtgui_freq_sink_x_0 = qtgui.freq_sink_c(
            1024, #size
            window.WIN_BLACKMAN_hARRIS, #wintype
            0, #fc
            samp_rate, #bw
            "", #name
            2,
            None # parent
        )
        self.qtgui_freq_sink_x_0.set_update_time(0.10)
        self.qtgui_freq_sink_x_0.set_y_axis(-140, 10)
        self.qtgui_freq_sink_x_0.set_y_label('Relative Gain', 'dB')
        self.qtgui_freq_sink_x_0.set_trigger_mode(qtgui.TRIG_MODE_FREE, 0.0, 0, "")
        self.qtgui_freq_sink_x_0.enable_autoscale(False)
        self.qtgui_freq_sink_x_0.enable_grid(False)
        self.qtgui_freq_sink_x_0.set_fft_average(1.0)
        self.qtgui_freq_sink_x_0.enable_axis_labels(True)
        self.qtgui_freq_sink_x_0.enable_control_panel(False)
        self.qtgui_freq_sink_x_0.set_fft_window_normalized(False)



        labels = ['', '', '', '', '',
            '', '', '', '', '']
        widths = [1, 1, 1, 1, 1,
            1, 1, 1, 1, 1]
        colors = ["blue", "red", "green", "black", "cyan",
            "magenta", "yellow", "dark red", "dark green", "dark blue"]
        alphas = [1.0, 1.0, 1.0, 1.0, 1.0,
            1.0, 1.0, 1.0, 1.0, 1.0]

        for i in range(2):
            if len(labels[i]) == 0:
                self.qtgui_freq_sink_x_0.set_line_label(i, "Data {0}".format(i))
            else:
                self.qtgui_freq_sink_x_0.set_line_label(i, labels[i])
            self.qtgui_freq_sink_x_0.set_line_width(i, widths[i])
            self.qtgui_freq_sink_x_0.set_line_color(i, colors[i])
            self.qtgui_freq_sink_x_0.set_line_alpha(i, alphas[i])

        self._qtgui_freq_sink_x_0_win = sip.wrapinstance(self.qtgui_freq_sink_x_0.qwidget(), Qt.QWidget)
        self.top_layout.addWidget(self._qtgui_freq_sink_x_0_win)
        self._gain_rx2_range = Range(0, 75, 0.1, 0, 200)
        self._gain_rx2_win = RangeWidget(self._gain_rx2_range, self.set_gain_rx2, "Gain RX2", "counter_slider", float, QtCore.Qt.Horizontal)
        self.top_layout.addWidget(self._gain_rx2_win)
        self._gain_rx0_range = Range(0, 75, 0.1, 0, 200)
        self._gain_rx0_win = RangeWidget(self._gain_rx0_range, self.set_gain_rx0, "Gain RX0", "counter_slider", float, QtCore.Qt.Horizontal)
        self.top_layout.addWidget(self._gain_rx0_win)
        self._freq_rftune_range = Range(70e6, 80e6, 25e3, 70e6, 200)
        self._freq_rftune_win = RangeWidget(self._freq_rftune_range, self.set_freq_rftune, "Freq RF Tune", "counter_slider", float, QtCore.Qt.Horizontal)
        self.top_layout.addWidget(self._freq_rftune_win)
        self.fft_bin_select_1 = fft_bin_select(
            fft_size=1024,
            nSkip=1,
            samp_rate=samp_rate,
        )
        self.fft_bin_select_0 = fft_bin_select(
            fft_size=1024,
            nSkip=1,
            samp_rate=samp_rate,
        )
        self.blocks_throttle_0 = blocks.throttle(gr.sizeof_gr_complex*1, samp_rate,True)
        self.blocks_nlog10_ff_2 = blocks.nlog10_ff(10, 1, 0)
        self.blocks_nlog10_ff_1 = blocks.nlog10_ff(10, 1, 0)
        self.blocks_nlog10_ff_0 = blocks.nlog10_ff(10, 1, 0)
        self.blocks_multiply_const_xx_0 = blocks.multiply_const_ff(180/np.pi, 1)
        self.blocks_divide_xx_0 = blocks.divide_cc(1)
        self.blocks_complex_to_mag_3 = blocks.complex_to_mag(1)
        self.blocks_complex_to_mag_2 = blocks.complex_to_mag(1)
        self.blocks_complex_to_mag_1 = blocks.complex_to_mag(1)
        self.blocks_complex_to_mag_0 = blocks.complex_to_mag(1)


        ##################################################
        # Connections
        ##################################################
        self.connect((self.blocks_complex_to_mag_0, 0), (self.blocks_multiply_const_xx_0, 0))
        self.connect((self.blocks_complex_to_mag_1, 0), (self.blocks_nlog10_ff_0, 0))
        self.connect((self.blocks_complex_to_mag_2, 0), (self.blocks_nlog10_ff_2, 0))
        self.connect((self.blocks_complex_to_mag_3, 0), (self.blocks_nlog10_ff_1, 0))
        self.connect((self.blocks_divide_xx_0, 0), (self.blocks_complex_to_mag_0, 0))
        self.connect((self.blocks_divide_xx_0, 0), (self.blocks_complex_to_mag_1, 0))
        self.connect((self.blocks_multiply_const_xx_0, 0), (self.qtgui_number_sink_0, 1))
        self.connect((self.blocks_nlog10_ff_0, 0), (self.qtgui_number_sink_0, 0))
        self.connect((self.blocks_nlog10_ff_1, 0), (self.qtgui_number_sink_1, 0))
        self.connect((self.blocks_nlog10_ff_2, 0), (self.qtgui_number_sink_1, 1))
        self.connect((self.blocks_throttle_0, 0), (self.qtgui_freq_sink_x_0, 0))
        self.connect((self.fft_bin_select_0, 0), (self.blocks_complex_to_mag_2, 0))
        self.connect((self.fft_bin_select_0, 0), (self.blocks_divide_xx_0, 1))
        self.connect((self.fft_bin_select_1, 0), (self.blocks_complex_to_mag_3, 0))
        self.connect((self.fft_bin_select_1, 0), (self.blocks_divide_xx_0, 0))
        self.connect((self.zeromq_pull_source_0, 0), (self.blocks_throttle_0, 0))
        self.connect((self.zeromq_pull_source_0, 0), (self.fft_bin_select_1, 0))
        self.connect((self.zeromq_pull_source_1, 0), (self.fft_bin_select_0, 0))
        self.connect((self.zeromq_pull_source_1, 0), (self.qtgui_freq_sink_x_0, 1))


    def closeEvent(self, event):
        self.settings = Qt.QSettings("GNU Radio", "holorx_client")
        self.settings.setValue("geometry", self.saveGeometry())
        self.stop()
        self.wait()

        event.accept()

    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate
        self.blocks_throttle_0.set_sample_rate(self.samp_rate)
        self.fft_bin_select_0.set_samp_rate(self.samp_rate)
        self.fft_bin_select_1.set_samp_rate(self.samp_rate)
        self.qtgui_freq_sink_x_0.set_frequency_range(0, self.samp_rate)

    def get_gain_rx2(self):
        return self.gain_rx2

    def set_gain_rx2(self, gain_rx2):
        self.gain_rx2 = gain_rx2
        self.xmlrpc_client_2.set_gain_rx2(self.gain_rx2)

    def get_gain_rx0(self):
        return self.gain_rx0

    def set_gain_rx0(self, gain_rx0):
        self.gain_rx0 = gain_rx0
        self.xmlrpc_client_1.set_gain_rx0(self.gain_rx0)

    def get_freq_rftune(self):
        return self.freq_rftune

    def set_freq_rftune(self, freq_rftune):
        self.freq_rftune = freq_rftune
        self.xmlrpc_client_0.set_freq_rftune(self.freq_rftune)

    def get_client_address(self):
        return self.client_address

    def set_client_address(self, client_address):
        self.client_address = client_address




def main(top_block_cls=holorx_client, options=None):

    if StrictVersion("4.5.0") <= StrictVersion(Qt.qVersion()) < StrictVersion("5.0.0"):
        style = gr.prefs().get_string('qtgui', 'style', 'raster')
        Qt.QApplication.setGraphicsSystem(style)
    qapp = Qt.QApplication(sys.argv)

    tb = top_block_cls()

    tb.start()

    tb.show()

    def sig_handler(sig=None, frame=None):
        tb.stop()
        tb.wait()

        Qt.QApplication.quit()

    signal.signal(signal.SIGINT, sig_handler)
    signal.signal(signal.SIGTERM, sig_handler)

    timer = Qt.QTimer()
    timer.start(500)
    timer.timeout.connect(lambda: None)

    qapp.exec_()

if __name__ == '__main__':
    main()
