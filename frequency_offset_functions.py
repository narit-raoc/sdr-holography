"""
Frequency offset correction

"""

import numpy as np
from scipy.fftpack import fft, fftshift
from scipy.signal import get_window


def peak_detection(fs, in_sig, fft_size, w=get_window('hamming', 512)):
    """
    Determines the maximum peak location of the given input signal samples.
    The peak is given by p = mX[k_o] when mX[k_o - 1] < mX[k_o] > mX[k_o + 1].
    Returns this as a tuple made up of the first element being the frequency
    bin of the peak value, second element of the tuple as magnitude of the peak,
    third element as interpolated peak bin and the fourth last element as the
    magnitude of the ipolated peak.

    Parameters
    ----------
    :param fs:
    :param in_sig: numpy array
    :param w: numpy array of the given window size
    :param fft_size: int
                FFT size of the output spectrum

    Returns
    -------
     : Tuple
        The first element is the bin of the spectral peak,
        The second element is the magnitude value of the peak
        The third element is the interpolated peak
        The fout=rth element is the magnitude of the interpolated peak
    """

    # Select beginning of signal to be same as window size
    wind_size = len(w)
    time = 0
    in_sig = in_sig[int(time * fs):int(time * fs) + wind_size]

    # positive frequencies side of the spectrum including the zero sample
    # positive_spectrum = (fft_size // 2 + 1)

    # Apply a window to the input signal
    windowed_sig = in_sig * w

    # Compute DFT using FFT and put DC at center using fftshift functiom
    in_sig_fft = fft(windowed_sig, fft_size)
    in_sig_fftshit = fftshift(in_sig_fft)

    # The parabolic interpolation only works correctly with the peak of
    # the main lobe in dB scale not linear
    in_sig_abs = abs(in_sig_fftshit)
    # replace zeros with epsilon to handle log
    in_sig_abs[in_sig_abs < np.finfo(float).eps] = np.finfo(float).eps
    in_sig_mag = 20 * np.log10(in_sig_abs)

    # Extract local maxima
    peak_mag = np.amax(in_sig_mag)
    # Frequency bin of the peak magnitude
    # peak_loc = np.where(in_sig_mag == peak_mag)[0][0]
    peak_loc = in_sig_mag.argmax()

    # -------------------------------------------------

    left_bin_mag = in_sig_mag[peak_loc-1]          # magnitude of bin at the left
    peak_bin_mag = in_sig_mag[peak_loc]             # magnitude of peak bin
    right_bin_mag = in_sig_mag[peak_loc+1]          # magnitude of bin at the right

    f = np.arange(-fft_size / 2, fft_size / 2) * fs / fft_size
    peak_loc = f[peak_loc]

    # center of parabola
    interpolated_peakloc = peak_loc + 0.5*(left_bin_mag-right_bin_mag)/(left_bin_mag-2*peak_bin_mag+right_bin_mag)

    # magnitude of peaks
    interpolated_mag = peak_bin_mag - 0.25 * (left_bin_mag-right_bin_mag) * (interpolated_peakloc-peak_loc)

    # peak_loc = (fs * peak_loc) / float(fft_size)
    # interpolated_peakloc = (fs * interpolated_peakloc) / float(fft_size)

    return peak_loc, peak_mag, interpolated_peakloc, interpolated_mag


def freq_shifting(freq, freq_shift, y, fs):
    """
    Use the given interpolated peak value to shift the spectrum of the input array
    to the center of the spectrum.

    Parameters
    ----------
    :param freq:int
        Frequency shift value to be commanded
    :param freq_shift:float
        Peak magnitude value
    :param fs:int
        Sampling frequency
    :param y:numpy array
        Values of the input signal

    Returns
    -------
    corrected_mX : ndarray
        The shifted array.
    """
    freq_offset = freq_shift - freq
    corrected_y = y * np.exp(-1j * 2 * np.pi * freq_offset * np.arange(len(y)) / fs)

    return corrected_y


if __name__ == "__main__":
    import matplotlib.pyplot as plt
    import signal_utils as util

    # Parameters
    length_signal = 6
    fs = 2e3
    freq1 = 20
    fft_size = 512  # FFT size

    # Generate test signals
    t, x = util.gen_cosine(fs, freq1, length_signal)

    #   Add some noise
    # x = x + util.gen_noise(fs, length_signal)

    # Applying some known frequency offset
    y = x * np.exp(-1j * 2 * np.pi * (80 / fs) * np.arange(len(x)))
    # y = y[0:512]
    # print(y)

    # Calculating frequency bins with FFT
    delta_f = fs / fft_size  # Frequency resolution
    sampleIndex = np.arange(-fft_size / 2, fft_size / 2)  # Raw index for FFT plot
    f = sampleIndex * delta_f  # frequencies

    # spectrum of original signal
    _, mX, _ = util.dft_comp(fs, x, fft_size)

    # Detecting bins of spectral peaks of the original signal
    peak_bin, peak_mag, interpolated_peak_bin, interpolated_mag = peak_detection(fs, x, fft_size)

    # spectrum of signal with frequency offset
    _, mY, _ = util.dft_comp(fs, y, fft_size)

    # Detecting bins of spectral peaks of the signal frequency offset
    peak_bin_y, peak_mag_y, interpolated_peak_bin_y, interpolated_mag_y = peak_detection(fs, y, fft_size)

    # Applying the frequency shift correction
    y_corrected = freq_shifting(freq1, interpolated_peak_bin_y, y, fs)

    # Spectrum of corrected signal
    _, mY_corrected, _ = util.dft_comp(fs, y_corrected, fft_size)

    # Spectral peak of the corrected signal
    *_, interpolated_peak_bin_corrected, interpolated_mag_corrected = peak_detection(fs, y_corrected, fft_size)

    plt.figure(1, clear=True)
    plt.subplot(1, 2, 1)
    plt.plot(f, mX)
    plt.plot(peak_bin, peak_mag, 'r-', marker='x', linestyle='')
    plt.plot(interpolated_peak_bin, interpolated_mag, 'r-', marker='o', linestyle='')

    plt.xlabel('Frequency [Hz]')
    plt.ylabel('|X| [dB]')
    plt.title('Magnitude Spectrum')
    plt.minorticks_on()
    plt.grid()

    plt.subplot(1, 2, 2)
    plt.plot(f, mY)
    plt.plot(peak_bin_y, peak_mag_y, 'r-', marker='x', linestyle='')
    plt.plot(interpolated_peak_bin_y, interpolated_mag_y, 'r-', marker='o', linestyle='')

    plt.xlabel('Frequency [Hz]')
    plt.ylabel('|X| [dB]')
    plt.title('Magnitude Spectrum')
    plt.minorticks_on()
    plt.grid()

    plt.figure(2, clear=True)
    plt.plot((fs / 2.0) * np.arange(-fft_size / 2, fft_size / 2) / float(fft_size / 2), mY_corrected)
    plt.plot(interpolated_peak_bin_corrected, interpolated_mag_corrected, 'r-', marker='x', linestyle='')

    plt.xlabel('Frequency [Hz]')
    plt.ylabel('|X| [dB]')
    plt.title('Magnitude Spectrum')
    plt.minorticks_on()
    plt.grid()

    plt.show()
