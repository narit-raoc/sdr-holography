"""
This module contains some basic signal processing
operations.

Common Ops;
- db            converting the linear power value to decibels
- snr           computing the signal-to-noise ratio of two signals
- rms           computes the root mean squared value of an array
- nexpow2       returns the next power of two of n (useful in FFT sizes)
-

Sample rate changes
- upsample      upsampling an array
- downsample    downsampleing an array


Filtering
- fftfilt       Apply an FIR filter to a signal using the overlap-add method.
- interpolate   upsample and filter
- decimate      filter and downsample
"""

import numpy as np
from scipy.fftpack import fft, fftshift
from scipy.signal import get_window


# ------------ Signal Generation ---------------


def gen_cosine(fs, freq, length_pulse_s, phi0=0):
    """
    Generate a pulse of the duration specified by length_pulse(seconds)
    at a specified frequency.

    Parameters
    ----------
    :param phi0: int
    :param length_pulse_s: int
    :param freq: int
    :param fs: int

    Returns
    -------
    t0, signal_c: Tuple
       """
    no_samples = int(length_pulse_s * fs)
    t0 = np.arange(0, no_samples) / fs

    signal_c = np.exp(1j * (2 * np.pi * freq * t0 + phi0))
    return t0, signal_c


# Generate noise signal
def gen_noise(fs, length_s):
    n_samples = int(length_s * fs)
    # tt = np.arange(0, n_samples) / fs
    noise = np.random.normal(size=n_samples) + 1j * np.random.normal(size=n_samples)
    return noise


# ---------- Common signal operattions ----------


def db(x):
    """ Convert linear value to dB value """
    return 10 * np.log10(x)


def snr(x, axis=0):
    """
    Documentation
    """
    # Calculate mean of the array
    # the mean is commonly called the DC value
    mu = x.mean(axis)

    # Calculate standard deviation
    # Standard dev is a measure of how far the signal fluctuates
    # from the mean. The variance represents the power of this fluctuation
    # variance = (x - x.mean())**2
    # stand_dev = sqrt(variance)
    stand_dev = x.std(axis)

    return np.where(stand_dev == 0, 0, mu / stand_dev)


# DFT calculation
def fft_x_shift(x, fft_size):
    return np.fft.fftshift(np.fft.fft(x, fft_size))


# ---------- Sample rate changes ----------
def upsample(x, L):
    """Takes input signal vector x and upsample it by inserting
    L-1 zeros between each input sample."""

    n = len(x)
    x_up = np.hstack((x.reshape(n, 1), np.zeros((n, int(L-1)))))
    x_up = x_up.flatten()
    return x_up


def downsample(x, M):
    """Takes input signal x vector and retains at the output
    every Mth sample of x."""
    # x_decim = np.where([i % M == 0 for i in x])
    # x_decim = x[x % M == 0]
    
    return x[::M]


# ---------- Fitering ----------


def dft_comp(fs, in_sig, fft_size, w=get_window('blackman', 512)):
    """
    Computes the dft of the input signal.
    Apply the chosen window to the input signal and computes
    dft and returns a tuple of the computed dft and magnitude
    and phase spectrum.

    Parameters
    ----------
    :param fs:
    :param in_sig: numpy array
        Input signal.
    :param w: numpy array of the given window size
    :param fft_size: int
                FFT size of the output spectrum

    Returns
    -------
    in_sig_fftshit, in_sig_mag, in_sig_phase : tuple
        The first element in the tupple is the fft of the input signal,
        The second element of the tuple is the magnitude spectrum (dB) and
        The third element is the phase spectrum of the input signal
    """
    # Select beginning of signal to be same as window size
    wind_size = len(w)
    time = 0
    in_sig = in_sig[int(time * fs):int(time * fs) + wind_size]

    windowed_sig = in_sig * w
    in_sig_fft = fft(windowed_sig, fft_size)
    in_sig_fftshit = fftshift(in_sig_fft)
    # compute the absolute value of positive frequencies
    in_sig_abs = abs(in_sig_fftshit)
    # replace zeros with epsilon to handle log
    in_sig_abs[in_sig_abs < np.finfo(float).eps] = np.finfo(float).eps

    # magnitude spectrum
    in_sig_mag = 20 * np.log10(in_sig_abs)
    # phase spectrum
    in_sig_phase = np.angle(in_sig_fft)

    return in_sig_fftshit, in_sig_mag, in_sig_phase


def fftfilt():
    pass


def interpolate():
    pass


def decimate():
    """
    Two for one function:
        - First low pass filters the signal to prevent aliasing
        - Then reduce the original sampling rate to a lower rate
    """ 
    pass


if __name__ == '__main__':
    import numpy as np

    x = np.random.randint(1, 10, 20)
    # x = np.arange(20)
    print(f'x: {x}')

    x_decim = downsample(x, 2)
    print(f'x_decim: {x_decim}')
