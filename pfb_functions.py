"""
This code is based on 'pfb_introduction' notebook from  the
https://github.com/telegraphic/pfb_introduction/blob/master/pfb_introduction.ipynb
by Danny C. Price. With some modifications of functions to design the prototype
 filter and also handling a case when the input signal is not a multiple of number
of taps times number of channels.
"""

# imports
from gnuradio.filter import window
from scipy.fftpack import fft, ifft
from gnuradio import filter, gr, fft, blocks
import numpy as np
import math
import matplotlib.pyplot as plt
from scipy.signal import get_window

plt.style.use('dark_background')


def prototype_filter(Fs, ch_bw, ch_tb, atten_db, gain=1, win=window.WIN_BLACKMAN):
    """Designing the prototype filter

    Args:
        gain: Filter gain in the passband (linear)
        Fs: Sampling rate (sps)
        ch_bw: End of pass band (in Hz)
        ch_tb: Start of stop band (in Hz)
        atten_db: Pass band ripple in dB (should be small, < 1)
        win: window type"""
    prototype_lpf = filter.firdes.low_pass_2(gain, Fs, ch_bw, ch_tb, atten_db, win)
    filt_taps = np.asarray(prototype_lpf).flatten()

    return filt_taps


def polyphase_filter(filt, decim):
    if filt.size % decim != 0:
        taps_per_channel = math.ceil(filt.size / decim)
        filt = np.append(filt, np.zeros(decim * taps_per_channel - filt.size))
    hp = filt.reshape((math.ceil(filt.size / decim), decim)).T
    return hp


def polyphase_analysis(input_signal, filt, decim):
    pfb = polyphase_filter(filt, decim)
    M = pfb.shape[1]

    W = int(input_signal.shape[0] / M / decim)

    input_signal = input_signal[0:W * M * decim]
    x_p = input_signal.reshape((W * M, decim)).T
    x_summed = np.zeros((decim, (M * W - M)), dtype=complex)
    for t in range(0, (M * W - M)):
        x_weighted = x_p[:, t:t + M] * pfb
        x_summed[:, t] = x_weighted.sum(axis=1)

    x_summed = x_summed.T
    x_pfb = fft_x(x_summed, decim)
    return x_pfb


def fft_x(x_p, decim, axis=1):
    return np.fft.fft(x_p, decim, axis=axis)


def gen_cosine(fs, freq, len_pulse, phi0=0):

    no_samples = int(len_pulse * fs)
    t = np.arange(0, no_samples) / fs
    x = np.exp(1j * (2 * np.pi * freq * t + phi0))

    return t, x


# Generate noise signal
def gen_noise(fs, length_s):
    n_samples = int(length_s * fs)
    # tt = np.arange(0, n_samples) / fs
    noise = np.random.normal(size=n_samples) + 1j * np.random.normal(size=n_samples)
    return noise


def db(x):
    """ Convert linear value to dB value """
    return 10 * np.log10(x)


def freq_scale(fs, N):
    # Calculating frequency bins with FFT
    delta_f = fs / N  # Frequency resolution
    # Raw index for FFT plot
    sampleIndex = np.arange(-N / 2, N / 2)
    # x=axis index converted to frequencies
    f = sampleIndex * delta_f

    return f


if __name__ == "__main__":
    fs = 100e3
    length_signal = 1
    freq1 = 1e3
    freq2 = 22e3
    freq3 = 44e3
    freq4 = -23e3
    freq5 = -44e3

    t, x1 = gen_cosine(fs, freq1, length_signal)
    _, x2 = gen_cosine(fs, freq2, length_signal)
    _, x3 = gen_cosine(fs, freq3, length_signal)
    _, x4 = gen_cosine(fs, freq4, length_signal)
    _, x5 = gen_cosine(fs, freq5, length_signal)

    x = x1 + x2 + x3 + x4 + x5

    # noise = gen_noise(fs, len(x))

    x = x + gen_noise(fs, length_signal)

    M = 501

    fft_size = 1024

    f = freq_scale(fs, fft_size)

    window = get_window('blackman', M)

    time = 0.01
    xw = x[int(time * fs):int(time * fs) + M]
    xw = xw * window

    X = np.fft.fftshift(np.fft.fft(xw, fft_size))

    # ------------------- PFB Implementation -------------------------------
    nchannels = 5
    gain = 1
    atten_dB = 60       # Pass band ripple in dB (should be small, < 1)
    ch_bw = fs / float(nchannels) / 3.0   # End of pass band (in Hz)
    ch_tb = fs / float(nchannels) / 5.0   # Start of stop band (in Hz)

    taps = prototype_filter(fs, ch_bw, ch_tb, atten_dB)
    print(f'Taps: {taps}')
    print(f'Length of filter: {taps.shape}')

    pfb_fir = polyphase_filter(taps, nchannels)
    print(f'pfb_fir: {pfb_fir}')

    pfb_anal = polyphase_analysis(xw, taps, nchannels)

    # pfb_anal = np.abs(pfb_anal) ** 2

    print(pfb_anal.shape)

    fs_decim = fs / nchannels

    f_pfb = freq_scale(fs_decim, fft_size)

    X_channel0 = np.fft.fftshift(np.fft.fft(pfb_anal[:, 0], fft_size))
    X_channel1 = np.fft.fftshift(np.fft.fft(pfb_anal[:, 1], fft_size))
    X_channel2 = np.fft.fftshift(np.fft.fft(pfb_anal[:, 2], fft_size))
    X_channel3 = np.fft.fftshift(np.fft.fft(pfb_anal[:, 3], fft_size))
    X_channel4 = np.fft.fftshift(np.fft.fft(pfb_anal[:, 4], fft_size))

    # ------------------ Plots ----------------------------

    plt.figure(1, clear=True)
    plt.plot(f, 20 * np.log10(abs(X)))
    plt.title('Input 100 kHz Spectrum')
    plt.xlabel('Frequency [Hz]')
    plt.ylabel('|X| [dB]')
    plt.grid()

    plt.figure(2, clear=True)
    plt.subplot(331)
    plt.plot(f_pfb, 20 * np.log10(abs(X_channel0)))
    plt.xlabel('Frequency [Hz]')
    plt.ylabel('|X| [dB]')
    plt.title('Channel 0: -10 to 10 kHz')
    plt.minorticks_on()
    plt.grid()

    plt.subplot(332)
    plt.plot(f_pfb, 20 * np.log10(abs(X_channel1)))
    plt.xlabel('Frequency [Hz]')
    plt.ylabel('|X| [dB]')
    plt.title('Channel 1: 10 to 30 kHz')
    plt.minorticks_on()
    plt.grid()
    plt.tight_layout()

    plt.subplot(333)
    plt.plot(f_pfb, 20 * np.log10(abs(X_channel2)))
    plt.xlabel('Frequency [Hz]')
    plt.ylabel('|X| [dB]')
    plt.title('Channel 2: 30 to 50 kHz')
    plt.minorticks_on()
    plt.grid()
    plt.tight_layout()

    plt.subplot(334)
    plt.plot(f_pfb, 20 * np.log10(abs(X_channel3)))
    plt.xlabel('Frequency [Hz]')
    plt.ylabel('|X| [dB]')
    plt.title('Channel 3: -50 to -30 kHz')
    plt.minorticks_on()
    plt.grid()
    plt.tight_layout()

    plt.subplot(335)
    plt.plot(f_pfb, 20 * np.log10(abs(X_channel4)))
    plt.xlabel('Frequency [Hz]')
    plt.ylabel('|X| [dB]')
    plt.title('Channel 4: -30 to -10 kHz')
    plt.minorticks_on()
    plt.grid()
    plt.tight_layout()

    plt.show()
